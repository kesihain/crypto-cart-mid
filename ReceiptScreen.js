import React, { useContext, useEffect, useState } from 'react';
import {cartContext} from './App'
import { Container, Header, Body, Title, Left, Icon, Right, Button, Content, Text } from 'native-base';
import Receipt from './Receipt'



export default function ReceiptScreen({navigation}){
    const {receipt} = useContext(cartContext);
    return(
        <Container>
            <Header>
                <Left>
                    {/* <TouchableOpacity transparent>
                    <Icon name='arrow-back'/>
                    </TouchableOpacity> */}
                </Left>
                <Body onPress={()=>navigation.navigate('Home')}>
                    <Title >Crypto Market</Title>
                </Body>
                <Right>
                    <Button onPress={()=>navigation.navigate('Cart')}>
                        <Icon name='cart'></Icon>
                    </Button>
                    <Button onPress={()=>navigation.navigate('Receipt')}>
                        <Icon name='paper'></Icon>
                    </Button>
                </Right>
            </Header>
            <Content>
                <Text style={{textAlign:'center',padding:10}}>You own</Text>
                {receipt.map((item)=>(
                    <Receipt key={item.id} item={item} ></Receipt>
                ))}
            </Content>
        </Container>
    )
}