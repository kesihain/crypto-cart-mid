import Axios from 'axios';
import { Card, CardItem, Text, Body, Right, Button } from 'native-base';
import React, { useContext, useEffect, useState } from 'react';
import { Image } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import {cartContext} from './App'

export default function CartCard({id}){
    const [coinData,setCoinData]=useState({});
    const [coinPrice,setCoinPrice]=useState(0);
    // const [coinImage,setCoinImage]=useState('');
    const [buyValue,setBuyValue]=useState(0);
    const {cart,setCart,receipt,setReceipt} = useContext(cartContext);
    useEffect(()=>{
        Axios({
            method:'GET',
            url:`https://api.coingecko.com/api/v3/coins/${id}`
        }).then(response=>{
            setCoinData(response.data)
            setCoinPrice(response.data.market_data.current_price.myr)
            setCoinImage(response.data.image.large)
        }).catch(error=>{
            console.log(error)
        })
    },[])

    const handleDelete = ()=>{
        setCart(cart.filter(item=>{
            return item != id
        }))
        
    }
    
    const handleBuy = ()=>{
        setReceipt([...receipt,{id:id,amount:buyValue/coinPrice}])
        setCart(cart.filter(item=>{
            return item != id
        }))
    }

    return (
        <Card>
            <CardItem>
                <Body>
                    <Text>{coinData.name}</Text>
                    <Text note>{coinData.symbol}</Text>
                </Body>
                <Right>
                    <Text>RM {coinPrice}</Text>
                </Right>
            </CardItem>
            <CardItem>
                <Body>
                    <TextInput placeholder="How much would you like to buy in RM" onChangeText={text=>setBuyValue(parseFloat(text))}></TextInput>
                </Body>
                <Right>
                    <Text>{buyValue?buyValue/coinPrice:0} coins to buy</Text>
                </Right>
            </CardItem>
            <CardItem>
                <Button onPress={handleBuy}>
                    <Text>Checkout {coinData.name}</Text>
                </Button>
            </CardItem>
            <CardItem>
                <Body>
                    <Button danger onPress={handleDelete}>
                        <Text>Delete from cart</Text>
                    </Button>
                </Body>
            </CardItem>
            {/* {coinImage?
            <CardItem>
                <Body>
                    <Image source={{uri:coinImage}} style={{height: 200, width: null, flex: 1}}></Image>
                </Body>
            </CardItem>:<></>
            } */}
        </Card>
    )
}