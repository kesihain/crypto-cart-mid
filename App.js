import * as Font from 'expo-font';
import { Header, Body, Title, Container, Left, Icon, Right, Button } from 'native-base';
import React, { useEffect, useState } from 'react';
import HomeScreen from './HomeScreen';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CartScreen from './CartScreen';
import ReceiptScreen from './ReceiptScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createStackNavigator();

export default function App() {

  
  const [cart,setCart] = useState([]);
  const [receipt,setReceipt] = useState([]);
  
  // AsyncStorage.getItem('@cart').then(asynCart=>{
  //   console.log(asynCart)
  //   setCart(JSON.parse(asynCart))
  // })
  return (
    <cartContext.Provider value={{cart,setCart,receipt,setReceipt}}>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen
              name='Home'
              component={HomeScreen}
            />
            <Stack.Screen
              name='Cart'
              component={CartScreen}
            />
            <Stack.Screen
              name='Receipt'
              component={ReceiptScreen}
            />
          </Stack.Navigator>
        </NavigationContainer>
    </cartContext.Provider>
  );
}

export const cartContext = React.createContext();

