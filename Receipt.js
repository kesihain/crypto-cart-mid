import Axios from 'axios';
import { Body, Card, CardItem, Right, Text } from 'native-base';
import React, { useContext, useEffect, useState } from 'react';

export default function Receipt(item){
    const [itemData,setItemData]=useState({});
    const [itemPrice,setItemPrice] = useState(0);
    useEffect(()=>{
        console.log(item.item.id)
        Axios({
            method:'GET',
            url:`https://api.coingecko.com/api/v3/coins/${item.item.id}`
        }).then(response=>{
            setItemData(response.data)
            setItemPrice(parseFloat(response.data.market_data.current_price.myr))
        }).catch(error=>{
            console.log(error)
        })
    },[])
    return(
        <Card>
            <CardItem>
                <Body>
                    <Text>{itemData.name}</Text>
                    <Text note>{item.item.amount} {itemData.symbol}</Text>
                </Body>
                <Right><Text>Value in RM : {itemPrice*item.item.amount}</Text></Right>
            </CardItem>
        </Card>
    )
}