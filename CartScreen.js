import React, { useContext, useEffect, useState } from 'react';
import Axios from 'axios';
import { Text } from 'react-native';
import { Container, Header, Body, Title, Left, Icon, Right, Button, Content } from 'native-base';
import { cartContext } from './App';
import CartCard from './CartCard'


export default function CartScreen({navigation}){
    const { cart, setCart } = useContext(cartContext);
    console.log(cart)
    return (
        <Container>
            <Header>
                <Left>
                    {/* <TouchableOpacity transparent>
                    <Icon name='arrow-back'/>
                    </TouchableOpacity> */}
                </Left>
                <Body onPress={()=>navigation.navigate('Home')}>
                    <Title >Crypto Market</Title>
                </Body>
                <Right>
                    <Button onPress={()=>navigation.navigate('Cart')}>
                        <Icon name='cart'></Icon>
                    </Button>
                    <Button onPress={()=>navigation.navigate('Receipt')}>
                        <Icon name='paper'></Icon>
                    </Button>
                </Right>
            </Header>
            <Content>
                {cart.map((id)=>(
                    <CartCard key={id} id={id}></CartCard>
                ))}
            </Content>
        </Container>
    )
}