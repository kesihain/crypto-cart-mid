# Crypto Cart

### Setup and Launch-quick testing
1. Clone or download repo.
2. Run `expo install`
3. Download expo mobile app
4. Run `expo start` and open displayed link in browser
5. Use tunnel to avoid firewall issues
5. Launch expo mobile app and scan QR code present on browser

### Requirements met:
- Users can search for a coin based on name (eg. bitcoin) or symbols (eg. BTC) and browse from a list of coins - displayed with current market price 
- Add a coin with amount to a shopping cart *Partially:* amount can only be added in shopping cart
- Shopping cart should allow them to edit line item amount and calculate total based on current market price
- Once the user is satisfied, they can "checkout" at market price
- Users can view past orders in a history section
- A simple and neat UI

### Requirements not met:
- If the user quits the app. They should be able to return and continue adding to the existing shopping cart.
- Please write test cases against the functions implemented.
