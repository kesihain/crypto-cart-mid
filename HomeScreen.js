import React, { useContext, useEffect, useState } from 'react';
import { Text, TouchableOpacity, View, FlatList, ToastAndroid } from 'react-native';
import Axios from 'axios';
import { Header, Body, Title, Container, Left, Icon, Right, Button, List, ListItem } from 'native-base';
import { TextInput } from 'react-native-gesture-handler';
import { cartContext } from './App';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function  HomeScreen({navigation}){
    const [coins,setCoins] = useState([]);
    const [searchCoins,setSearchCoins] = useState([]);
    const {cart,setCart} = useContext(cartContext);

    const handleChange=(text)=>{
        setSearchCoins(coins.filter(item=>{
            return item.name.toLowerCase().match(text)
        }),...coins.filter(item=>{
            return item.symbol.toLowerCase().match(text)
        }))
    }

    const handlePress=(item)=>{
        setCart([...cart,item.id])
        // let store = [...cart,item.id]
        // async storeCart(key,item){
        //     try {
        //         await AsyncStorage.setItem(key,JSON.stringify([...cart,item.id]))
        //     } catch (error) {
        //         console.log(error)
        //     }
        // }
        // AsyncStorage.setItem('@cart',JSON.stringify([...cart,item.id]))
        ToastAndroid.show(`${item.name} added to cart`,ToastAndroid.SHORT)
    }
    
    const renderItem = ({item})=>(
        <ListItem key={item.id} onPress={()=>handlePress(item)}>
            <Left>
                <Text>{item.name}</Text>
            </Left>
            <Body>
            <Text>{item.symbol}</Text>
            </Body>
            <Right>
            <CoinPrice coinID={item.id}></CoinPrice>
            </Right>
        </ListItem>
    )


    useEffect(()=>{
      Axios({
        method: 'GET',
        url:'https://api.coingecko.com/api/v3/coins/list'
      }).then(response=>{
        setCoins(response.data)
      }).catch(error=>{
        console.log(error)
      })
    },[])
    return(
        <Container>
            <Header>
                <Left>
                    {/* <TouchableOpacity transparent>
                    <Icon name='arrow-back'/>
                    </TouchableOpacity> */}
                </Left>
                <Body onPress={()=>navigation.navigate('Home')}>
                    <Title >Crypto Market</Title>
                </Body>
                <Right>
                    <Button onPress={()=>navigation.navigate('Cart')}>
                        <Icon name='cart'></Icon>
                    </Button>
                    <Button onPress={()=>navigation.navigate('Receipt')}>
                        <Icon name='paper'></Icon>
                    </Button>
                </Right>
            </Header>
            <View style={{padding:10}}>
                <TextInput
                    placeholder='Type here to search'
                    onChangeText={text=>handleChange(text.toLowerCase())}
                    defaultValue=''
                ></TextInput>
            </View>
            <List>
            <ListItem>
                <Left>
                <Text>Coin Name</Text>
                </Left>
                <Body>
                <Text>Coin Symbol</Text>
                </Body>
                <Right>
                <Text>Coin Price</Text>
                </Right>
            </ListItem>
            </List>
            {searchCoins==''?
                <FlatList
                data={coins}
                keyExtractor={item=>item.id}
                renderItem={renderItem}
                />:
                <FlatList
                data={searchCoins}
                keyExtractor={item=>item.id}
                renderItem={renderItem}
                />}
        </Container>
    );
}


export function CoinPrice({coinID}){
const [price,setPrice]=useState(0);
useEffect(()=>{
    Axios({
    method:'GET',
    url:`https://api.coingecko.com/api/v3/coins/${coinID}`
    }).then(response=>{
    setPrice(response.data.market_data.current_price.myr)
    }).catch(error=>{
    console.log(error)
    })
},[])
return(
    price?<Text>{price}</Text>:<Text>Unknown</Text>
)
}