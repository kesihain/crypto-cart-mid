import React from 'react';
import { Header, Body, Title, Left, Icon, Right, Button } from 'native-base';


export default function CartHeader(navigation){
    return (
        <Header>
        <Left>
            {/* <TouchableOpacity transparent>
            <Icon name='arrow-back'/>
            </TouchableOpacity> */}
        </Left>
        <Body onPress={()=>navigation.navigate('Home')}>
            <Title >Crypto Market</Title>
        </Body>
        <Right>
            <Button onPress={()=>navigation.navigate('Cart')}>
                <Icon name='cart'></Icon>
            </Button>
            <Button>
                <Icon name='paper'></Icon>
            </Button>
        </Right>
    </Header>
    )
}